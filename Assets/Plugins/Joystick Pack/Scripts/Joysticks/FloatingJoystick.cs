﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : Joystick
{
    public RectTransform m_Rect;
    protected override void Start()
    {
        m_Rect = GetComponent<RectTransform>();
        base.Start();
        float r = (float)Screen.width / (float)Screen.height;
        string _r = r.ToString("F2");
        string ratio = _r.Substring(0, 4);
        if(ratio == "1.60") {
            m_Rect.sizeDelta = new Vector2(800, 500);
        }else if(ratio == "1.33") {
            m_Rect.sizeDelta = new Vector2(800, 600);
        }
        else if (ratio == "1.77") {
            m_Rect.sizeDelta = new Vector2(800, 450);
        }
            background.gameObject.SetActive(false);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
        background.gameObject.SetActive(true);
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        background.gameObject.SetActive(false);
        base.OnPointerUp(eventData);
    }
}