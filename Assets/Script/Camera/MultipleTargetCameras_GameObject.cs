﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
[RequireComponent(typeof(Camera))]
public class MultipleTargetCameras_GameObject : MonoBehaviour
{
    public static MultipleTargetCameras_GameObject m_Instance;
    public List<Transform> m_Targets;
    public Camera m_ActiveCamera;
    public Vector3 m_Offset;
    public float m_SmoothTime;

    public float m_MinZoom;
    public float m_MaxZoom;
    public float m_ZoomLimiter;

    public Vector3 m_Velocity;

    Vector3 m_StartPosition;

    private void Awake() {
        m_Instance = this;
    }

    private void Start() {
        m_StartPosition = transform.position;
    }
    private void FixedUpdate() {
        if (!Game_Manager.m_Instance.m_IsStarted) return;
        if (m_Targets.Count == 0) return;
        f_Move();
        f_Zoom();

    }

    void f_Zoom() {
        float m_NewZoom = Mathf.Lerp(m_MaxZoom, m_MinZoom, f_GetGreatesDistance() / m_ZoomLimiter);
        m_ActiveCamera.orthographicSize = Mathf.Lerp(m_ActiveCamera.orthographicSize, m_NewZoom, Time.deltaTime * 200);
    }

    void f_Move() {
        Vector3 m_CenterPoint = f_GetCenterPoint();

        Vector3 m_NewPosition = m_CenterPoint + m_Offset;
        transform.position = Vector3.SmoothDamp(transform.position, m_NewPosition, ref m_Velocity, m_SmoothTime);
    }

    float f_GetGreatesDistance() {
        Bounds m_Bound = new Bounds(m_Targets[0].position, Vector3.zero);
        for (int i = 0; i < m_Targets.Count; i++) {
            m_Bound.Encapsulate(m_Targets[i].position);
        }

        return Mathf.Sqrt(Mathf.Pow(m_Bound.size.x, 2) + Mathf.Pow(m_Bound.size.y, 2));
    }
    Vector3 f_GetCenterPoint() {
        if(m_Targets.Count == 1) {
            return m_Targets[0].position;
        }

        Bounds m_Bound = new Bounds(m_Targets[0].position, Vector3.zero);

        for(int i = 0; i < m_Targets.Count; i++) {
            m_Bound.Encapsulate(m_Targets[i].position);
        }

        return m_Bound.center;
    }

    public IEnumerator<float> ie_CameraZoomOut() {
        do { 
            m_ActiveCamera.orthographicSize = Mathf.MoveTowards(m_ActiveCamera.orthographicSize, 25f, Time.deltaTime * 3f);
            yield return Timing.WaitForOneFrame;
        } while (m_ActiveCamera.orthographicSize != 25f);
    }

    public IEnumerator<float> f_ResetCamera() {
        do {
            transform.position = Vector3.MoveTowards(transform.position, m_StartPosition, Time.deltaTime * 5f);
            m_ActiveCamera.orthographicSize = Mathf.MoveTowards(m_ActiveCamera.orthographicSize, 25f, Time.deltaTime * 3f);
            yield return Timing.WaitForOneFrame;
        } while (m_ActiveCamera.orthographicSize != 25f || transform.position != m_StartPosition);
        //m_ActiveCamera.orthographicSize = 25f;
        //transform.position = m_StartPosition;
    }
}
