﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Child_Movement : Snake_Movement{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public SpriteRenderer m_Sprite;
    public Head_Movement m_RootHead;
    //===== PRIVATES =====
    float m_Distance;
    Vector3 m_Distances;
    float m_Angle;
    Quaternion t_Rotation;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        m_Rb = GetComponent<Rigidbody2D>();
        m_Sprite = GetComponent<SpriteRenderer>();
    }

    void Update() {
        m_Distance = Vector2.Distance(m_Head.transform.position, transform.position);
        m_Distances = m_Head.transform.position - transform.position;
        m_Rb.velocity = transform.up * 5 * (m_Distance >= 1 ? 1 : (m_Distance < 1 && m_Distance >= 0.9f) ? m_Distance : Mathf.MoveTowards(m_Distance, 0, 1));
        m_Angle = (Mathf.Atan2(m_Distances.y, m_Distances.x) * Mathf.Rad2Deg) - 90;
        t_Rotation = Quaternion.AngleAxis(m_Angle, Vector3.forward);
        transform.rotation = t_Rotation;
    }

    private void OnDisable() {
        SmokePooling_Manager.m_Instance.f_SpawnObject(SmokePooling_Manager.m_Instance.m_PoofObject, SmokePooling_Manager.m_Instance.m_Parent, this.transform.position, Quaternion.identity);
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Initialize(int p_SpriteOrder, Vector3 p_Scale, Head_Movement p_RootHead) {
        m_Sprite.sortingOrder = -p_SpriteOrder;
        m_ID = p_SpriteOrder;
        transform.localScale = p_Scale;
        m_RootHead = p_RootHead;
    }

    public void OnCollisionEnter2D(Collision2D p_Collision) {
        if(p_Collision.gameObject.tag == "Head") {
            for(int i = m_RootHead.m_ListChilds.IndexOf(this); i< m_RootHead.m_ListChilds.Count;i++) {
                m_RootHead.m_ListChilds[i].gameObject.SetActive(false);
            }
            m_RootHead.m_ListChilds.RemoveRange(m_RootHead.m_ListChilds.IndexOf(this), m_RootHead.m_ListChilds.Count - m_RootHead.m_ListChilds.IndexOf(this));
            m_RootHead.m_Score = m_RootHead.m_ListChilds.Count;
            Game_Manager.m_Instance.f_CheckLead(m_RootHead.m_ID, m_RootHead.m_Score);
        }
    }
}
