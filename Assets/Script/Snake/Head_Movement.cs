﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;

public class Head_Movement : Snake_Movement{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("General")]
    public List<Child_Movement> m_ListChilds;
    public Transform m_Parent;
    public float m_Score;
    public List<float> m_Scores;
    public e_PlayerState m_PlayerState;

    [Header("Movement")]
    Vector2 m_StartPosition;
    Quaternion m_StartRotation;
    Vector2 m_StartDirection;
    public float m_DirectionX;
    public float m_DirectionY;
    public Vector2 m_SavedDirection;

    [Header("UI")]
    public GameObject m_WinningBlink;
    public FloatingJoystick m_Joystick;
    //===== PRIVATES =====
    float t_Score;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_StartPosition = transform.position;
        m_StartRotation = transform.rotation;
        m_StartDirection = new Vector2(m_DirectionX, m_DirectionY);
    }

    void Start() {
        m_Rb = GetComponent<Rigidbody2D>();
       
    }
     
    private void OnEnable() {
        SmokePooling_Manager.m_Instance.f_SpawnObject(SmokePooling_Manager.m_Instance.m_PoofObject, SmokePooling_Manager.m_Instance.m_Parent, this.transform.position, Quaternion.identity);
    }
    private void OnDisable() {
        if (isActiveAndEnabled) {
            SmokePooling_Manager.m_Instance.f_SpawnObject(SmokePooling_Manager.m_Instance.m_PoofObject, SmokePooling_Manager.m_Instance.m_Parent, this.transform.position, Quaternion.identity);
        }
    }

    void Update() {
        if (!Game_Manager.m_Instance.m_IsStarted) {
            m_Rb.velocity = Vector3.zero;
            m_Rb.angularVelocity = 0f;
            return;
        }
        if (m_Joystick.Horizontal != 0) {
            m_DirectionX = m_Joystick.Horizontal;
        }
        if (m_Joystick.Vertical != 0) {
            m_DirectionY = m_Joystick.Vertical;
        }
        
        m_SavedDirection = new Vector2(m_DirectionX, m_DirectionY);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, m_SavedDirection);//Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, 0f, -Mathf.Atan2(m_SavedDirection.x, m_SavedDirection.y) * Mathf.Rad2Deg), Time.deltaTime);

        m_Rb.velocity = transform.up * 5f;
    }

    private void OnTriggerExit2D(Collider2D p_Collision) {
        if(p_Collision.gameObject.tag == "Food") {
            if(m_ListChilds.Count == 0) {
                m_ListChilds.Add(ChildPooling_Manager.m_Instance.f_SpawnObject(ChildPooling_Manager.m_Instance.m_ChildObject[m_ID], m_Parent, new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), Quaternion.identity));
                m_ListChilds[m_ListChilds.Count - 1].m_Head = this;
            }
            else {
                m_ListChilds.Add(ChildPooling_Manager.m_Instance.f_SpawnObject(ChildPooling_Manager.m_Instance.m_ChildObject[m_ID], m_Parent, new Vector3(m_ListChilds[m_ListChilds.Count - 1].transform.position.x, m_ListChilds[m_ListChilds.Count - 1].transform.position.y - 1, m_ListChilds[m_ListChilds.Count - 1].transform.position.z), Quaternion.identity));
                m_ListChilds[m_ListChilds.Count - 1].m_Head = m_ListChilds[m_ListChilds.Count - 2];
            }
            m_ListChilds[m_ListChilds.Count - 1].f_Initialize(m_ListChilds.Count, new Vector3(0.2f, 0.2f, 0.2f), this);
            m_Score = m_ListChilds.Count;
            Game_Manager.m_Instance.f_CheckLead(m_ID, m_Score);
            p_Collision.gameObject.SetActive(false);
            Game_Manager.m_Instance.f_SpawnFood();
        }
    }

    private void OnCollisionEnter2D(Collision2D p_Collision) {
        if(p_Collision.gameObject.tag == "Child" || p_Collision.gameObject.tag == "Head") {
            Game_Manager.m_Instance.m_IsAlive[m_ID] = false;
            f_RemoveChild();
            Game_Manager.m_Instance.f_CheckLead(m_ID, m_Score);
            Timing.RunCoroutine(co_Respawn());
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    IEnumerator<float> co_Respawn() {
        gameObject.SetActive(false);
        m_Score = 0;
        m_WinningBlink.SetActive(false);
        yield return Timing.WaitForOneFrame;
        transform.position = m_StartPosition;
        gameObject.SetActive(true);
    }

    public void f_RemoveChild() {
        for (int i = 0; i < m_ListChilds.Count; i++) {
            m_ListChilds[i].gameObject.SetActive(false);
        }

        m_ListChilds.Clear();
    }

    public void f_Reset() {
        gameObject.SetActive(true);
        m_Score = 0;
        f_RemoveChild();
        m_WinningBlink.SetActive(false);
        transform.position = m_StartPosition;
        transform.rotation = m_StartRotation;
        m_SavedDirection = Vector2.zero;
        m_DirectionX = m_StartDirection.x;
        m_DirectionY = m_StartDirection.y;
        m_Rb.velocity = Vector3.zero;
        m_Rb.angularVelocity = 0;
    }

    public float f_GetScore() {
        t_Score = 0;
        for(int i = 0; i < m_Scores.Count; i++) {
            t_Score += m_Scores[i];
        }
        return t_Score;
    }
}
