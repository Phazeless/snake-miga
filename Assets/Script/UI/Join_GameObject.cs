﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Join_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Join_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public Sprite m_JoinedImage;
    public Sprite m_NotJoinedImage;
    public Button m_InCircle;
    public GameObject m_MigaDummy;
    public GameObject m_Poof;
    public bool m_ToogleJoin;
    public Transform m_CancelTransform;
    public Transform m_JoinTransform;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_MoveToOutside() {
        if (m_MigaDummy.activeSelf) {
            m_Poof.SetActive(true);
            m_MigaDummy.SetActive(false);
        }
        m_InCircle.image.raycastTarget = false;
        iTween.MoveTo(m_InCircle.gameObject, iTween.Hash("position", m_CancelTransform.position, "time", 1f));
    }
    public void f_MoveToInside() {
        iTween.MoveTo(m_InCircle.gameObject, iTween.Hash("position", m_JoinTransform.position, "time", .5f));
    }

    public void f_Join() {
        m_Poof.SetActive(true);
        m_ToogleJoin = !m_ToogleJoin;
        Spawn_Manager.m_Instance.f_Join(m_ID, m_ToogleJoin);
        iTween.MoveTo(m_InCircle.gameObject, iTween.Hash("position", m_CancelTransform.position, "time", .5f, "oncomplete", "f_OnCompletedCancel", "oncompletetarget", gameObject));
    }

    public void f_OnCompletedCancel() {
        //DIBALIK SAMA YANG ATAS
        if (m_ToogleJoin) {
            m_MigaDummy.SetActive(true);
            m_InCircle.image.sprite = m_JoinedImage;
        }
        else {
            m_MigaDummy.SetActive(false);
            m_InCircle.image.sprite = m_NotJoinedImage;
        }
       
        iTween.MoveTo(m_InCircle.gameObject, iTween.Hash("position", m_JoinTransform.position, "time", .5f, "oncomplete", "f_OnCompletedJoin", "oncompletetarget", gameObject));
    }

    public void f_OnCompletedJoin() {
        
    }

    public void f_Reset() {
        m_InCircle.image.raycastTarget = true;
    }

    
}
