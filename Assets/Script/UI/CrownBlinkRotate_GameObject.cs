﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CrownBlinkRotate_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_StartClockwise;
    public float m_Speed = 1f;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        if (m_StartClockwise) f_RotateClockWise();
        else f_RotateCounterClockwise();
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_RotateClockWise() {
        iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(0, 0, 5), "speed", m_Speed, "oncomplete", "f_RotateCounterClockwise", "oncompletetarget", gameObject, "easetype", iTween.EaseType.linear));
    }

    public void f_RotateCounterClockwise() {
        iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(0, 0, -5), "speed", m_Speed, "oncomplete", "f_RotateClockWise", "oncompletetarget", gameObject, "easetype", iTween.EaseType.linear));
    }
}
