﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using MEC;

public class TransitionCover_Gameobject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static TransitionCover_Gameobject m_Instance;
    //===== STRUCT =====
    //===== PUBLIC =====
    public GameObject m_Loading;
    [Header("Open Settings")]
    public Transform m_OpenTarget;
    public float m_OpenDelay = 1f;
    public float m_OpenDuration = 1f;
    public iTween.EaseType m_OpenEaseType = iTween.EaseType.easeOutCirc;
    public c_Event m_OnOpenDone;
    [Header("Close Settings")]
    public Transform m_CloseTarget;
    public Transform m_CloseTransformStart;
    public float m_CloseDelay = 1f;
    public float m_CloseDuration = 1f;
    public iTween.EaseType m_CloseEaseType;
    public c_Event m_OnCloseDone;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        f_Open();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Open() {
        iTween.MoveTo(gameObject, iTween.Hash("position", m_OpenTarget.position, "delay", m_OpenDelay, "time", m_OpenDuration, "easetype", m_OpenEaseType, "oncomplete", "f_OnStartComplete","onstart","f_OnStart"));
    }

    public void f_Close() {
        if(m_CloseTransformStart != null) transform.position = m_CloseTransformStart.position;
        iTween.MoveTo(gameObject, iTween.Hash("position", m_CloseTarget.position, "delay", m_CloseDelay, "time", m_CloseDuration, "easetype", m_CloseEaseType, "oncomplete", "f_OnCloseComplete"));
    }

    public void f_OnStart() {
        m_Loading.SetActive(false);
    }

    public void f_OnStartComplete() {
        m_OnOpenDone?.Invoke();
    }

    public void f_OnCloseComplete() {   
        m_Loading.SetActive(true);
        Timing.RunCoroutine(ie_OnClose());
    }

    IEnumerator<float> ie_OnClose() {
        yield return Timing.WaitForOneFrame;
        m_OnCloseDone?.Invoke();
    }
}
