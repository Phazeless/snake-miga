﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Start_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Start_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_RotateSpeed = 10f;
    public Sprite m_Round2Start;
    public Sprite m_Round3Start;
    public Image m_Image;
    public Button m_Buttons;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
        //gameObject.SetActive(false);
    }
    private void OnEnable() {
        //iTween.Stop(gameObject);
        iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 1f);
        m_Buttons.interactable = true;
    }

    void Start(){
        m_Buttons = GetComponent<Button>();
    }

    void Update(){
        transform.Rotate(Vector3.forward * m_RotateSpeed * Time.deltaTime);
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Disable() {
        iTween.Stop(gameObject);
        m_Buttons.interactable = false;
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", .5f, "oncomplete", "f_OnCompleteScaleDown", "oncompletetarget", gameObject));
    }

    public void f_OnCompleteScaleDown() {
        gameObject.SetActive(false);
    }

    public void f_ChangeStartButton() {
        if (Game_Manager.m_Instance.m_Round == 2) {
            m_Image.sprite = m_Round2Start;
        }
        else if (Game_Manager.m_Instance.m_Round == 3) {
            m_Image.sprite = m_Round3Start;
        }
    }
}
