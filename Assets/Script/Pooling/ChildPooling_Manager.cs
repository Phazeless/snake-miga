﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectPooler;

public class ChildPooling_Manager : PoolingManager_Manager<Child_Movement>
{
    public static ChildPooling_Manager m_Instance;
    public Child_Movement[] m_ChildObject;

    private void Awake() {
        m_Instance = this;
    }

    public override bool f_AdditionalValidation(int p_Index, Child_Movement p_Object) {
        return p_Object.m_ID == m_PoolingContainer[p_Index].m_ID;
    }
}
