﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectPooler;

public class SmokePooling_Manager : PoolingManager_Manager<Smoke_GameObject>
{
    public static SmokePooling_Manager m_Instance;
    public Smoke_GameObject m_PoofObject;
    public Transform m_Parent;
    private void Awake() {
        m_Instance = this;
    }

}
