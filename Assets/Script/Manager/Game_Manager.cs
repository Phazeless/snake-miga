﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using MEC;
using Enumeration;
using TMPro;

[System.Serializable]
public class c_Event : UnityEvent
{

}

public class Game_Manager : MonoBehaviour
{   
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Game_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Game System")]
    public int m_Round;
    public int m_MaxRound;
    public float m_RoundTimer;
    public bool m_IsStarted => m_GameState == e_GameState.IsPlaying;
    public e_GameState m_GameState;

    [Header("Snake Information")]
    public Transform m_LeftBottom;
    public Transform m_RightTop;
    public List<bool> m_IsAlive;
    public List<Head_Movement> m_ListSnakes;

    [Header("UI")]
    public GameObject m_RightTimerObject;
    public GameObject m_LeftTimerObject;
    public TextMeshProUGUI m_RightTimer;
    public TextMeshProUGUI m_LeftTimer;
    public Image m_WaktuHabisImage;
    //===== PRIVATES =====
    Food_GameObject m_Food;
    float t_Max;
    float t_SameScore;
    int m_HighScoreSnakeIndex;
    float t_Timer;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_Instance = this;

    }

    void Start() {
        t_Timer = m_RoundTimer;

        TransitionCover_Gameobject.m_Instance.m_OnOpenDone.RemoveAllListeners();
        TransitionCover_Gameobject.m_Instance.m_OnCloseDone.RemoveAllListeners();
        //f_NextRound();
    }

    void Update() {
        m_RightTimer.text = "" + Mathf.Ceil(m_RoundTimer);
        m_LeftTimer.text = "" + Mathf.Ceil(m_RoundTimer);
        if(m_GameState == e_GameState.IsPlaying) {
            m_RoundTimer -= Time.deltaTime;
            if(m_RoundTimer <= 0) {
                m_RoundTimer = 0;
                m_GameState = e_GameState.IsNotPlaying;
                f_NextRound();
            }
        }
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_SpawnFood() {
        m_Food = FoodPooling_Manager.m_Instance.f_SpawnObject(FoodPooling_Manager.m_Instance.m_FoodObject, transform,
            new Vector3(Random.Range(m_LeftBottom.position.x, m_RightTop.position.x)
            , Random.Range(m_LeftBottom.position.y, m_RightTop.position.y),
            Random.Range(m_LeftBottom.position.z, m_RightTop.position.z)),
            Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
        m_Food.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
    }

    public void f_RepositionPlayer() {
        for(int i = 0; i < m_ListSnakes.Count; i++) {
            if(m_ListSnakes[i].m_PlayerState == e_PlayerState.Joined) {
                m_ListSnakes[i].f_Reset();

                if (!MultipleTargetCameras_GameObject.m_Instance.m_Targets.Contains(m_ListSnakes[i].transform)) {
                    MultipleTargetCameras_GameObject.m_Instance.m_Targets.Add(m_ListSnakes[i].transform);
                }
                
            }
        }
    }

    public void f_CheckLead(int p_ID, float p_Score) {   
        t_Max = p_Score;
        t_SameScore = 0;
        m_HighScoreSnakeIndex = p_ID;
        for (int i = 0; i < m_ListSnakes.Count; i++) {
            if (i == p_ID) continue;
            if(t_Max < m_ListSnakes[i].m_Score) {
                t_Max = m_ListSnakes[i].m_Score;
                m_HighScoreSnakeIndex = i;
            }
            else if(t_Max == m_ListSnakes[i].m_Score) {
                t_SameScore++;
            }
        }

        if(t_SameScore == 3) {
            for (int i = 0; i < m_ListSnakes.Count; i++) {
                m_ListSnakes[i].m_WinningBlink.SetActive(false);
            }
            return;
        }

        if (t_Max > p_Score || (t_Max == p_Score && m_HighScoreSnakeIndex == p_ID)) {
            for (int i = 0; i < m_ListSnakes.Count; i++) {
                m_ListSnakes[i].m_WinningBlink.SetActive(false);
            }
            m_ListSnakes[m_HighScoreSnakeIndex].m_WinningBlink.SetActive(true);
        } 
    }

    public void f_NextRound() {
        for(int i = 0; i < m_ListSnakes.Count; i++) {
            m_ListSnakes[i].m_Scores[m_Round - 1] = m_ListSnakes[i].m_Score;
            //m_ListSnakes[i].f_RemoveChild();
        }
        m_Round++;
        Spawn_Manager.m_Instance.f_ToogleJoystick(false);
        Timing.KillCoroutines();
        Timing.RunCoroutine(ie_NextGame());
    }

    public IEnumerator<float> ie_NextGame() {
        m_WaktuHabisImage.gameObject.SetActive(true);
        AudioManager_Manager.m_Instance.f_PlayOneshot(Spawn_Manager.m_Instance.m_TimesUpClip);
        yield return Timing.WaitForSeconds(1f);
        TransitionCover_Gameobject.m_Instance.f_Close();
        yield return Timing.WaitForSeconds(1f);
        m_RoundTimer = t_Timer;
        if (m_Round <= m_MaxRound) {
            Start_GameObject.m_Instance.f_ChangeStartButton();
            Start_GameObject.m_Instance.gameObject.SetActive(true);
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(MultipleTargetCameras_GameObject.m_Instance.f_ResetCamera()));
            Spawn_Manager.m_Instance.f_Reset();
            f_RepositionPlayer();
            yield return Timing.WaitForSeconds(1f);
            m_WaktuHabisImage.gameObject.SetActive(false);
            TransitionCover_Gameobject.m_Instance.f_Open();
        }
        else {
            m_RightTimerObject.SetActive(false);
            m_LeftTimerObject.SetActive(false);
            f_RepositionPlayer();
            yield return Timing.WaitForSeconds(2f);
            m_WaktuHabisImage.gameObject.SetActive(false);
            TransitionCover_Gameobject.m_Instance.m_OnOpenDone.AddListener(ResultManager_Manager.m_Instance.f_StartResultAppear);
            TransitionCover_Gameobject.m_Instance.m_OnCloseDone.AddListener(SceneManager_Manager.m_Instance.f_LoadMenu);

            TransitionCover_Gameobject.m_Instance.f_Open();
            //ResultManager_Manager.m_Instance.f_StartResultAppear();
        }

    }

  
}
