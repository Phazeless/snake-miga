﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using UnityEngine.UI;
using Enumeration;

public class Spawn_Manager : MonoBehaviour
{
    public static Spawn_Manager m_Instance;
    public List<Head_Movement> m_SnakeList;
    public List<FloatingJoystick> m_JoystickList;
    public List<Join_GameObject> m_JoinButtonObject;
    public int m_PlayerJoined;
    public bool m_CanStart => m_PlayerJoined > 1;

    [Header("UI")]
    public Start_GameObject m_StartButton;
    public List<Sprite> m_CountDown;
    public Image m_CountDownImage;
    public Animator m_CountDownAnimator;
    public Image m_Mulai;

    [Header("AudioClip")]
    public AudioClip m_ReadyClip;
    public AudioClip m_TickingClip;
    public AudioClip m_TimesUpClip;
    public AudioClip m_PointsGained;
    public AudioClip m_PointsLost;
    private void Awake() {
        m_Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void f_Join(int p_PlayerID, bool p_Toogle) {
        if (p_Toogle) {
            m_SnakeList[p_PlayerID].m_PlayerState = Enumeration.e_PlayerState.Joined;
            m_PlayerJoined++;
        }
        else {
            m_SnakeList[p_PlayerID].m_PlayerState = Enumeration.e_PlayerState.NotJoined;
            m_PlayerJoined--;
        }

        if (m_CanStart) {
            m_StartButton.gameObject.SetActive(true);
        }
        else {
            m_StartButton.f_Disable();
        }
        
    }

    public void f_StartGame() {
        m_StartButton.f_Disable();
        Timing.RunCoroutine(ie_CountDown());
    }

    public IEnumerator<float> ie_CountDown() {
        for (int i = 0; i < m_JoinButtonObject.Count; i++) {
            m_JoinButtonObject[i].f_MoveToOutside();
        }
        yield return Timing.WaitForSeconds(1);
        
        Timing.RunCoroutine(MultipleTargetCameras_GameObject.m_Instance.ie_CameraZoomOut());
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_TickingClip);
        m_CountDownImage.sprite = m_CountDown[2];
        m_CountDownImage.gameObject.SetActive(true);
        
        yield return Timing.WaitForSeconds(1);
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_TickingClip);
        m_CountDownImage.sprite = m_CountDown[1];
        m_CountDownAnimator.SetTrigger("Count");
        
        yield return Timing.WaitForSeconds(1);
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_TickingClip);
        m_CountDownImage.sprite = m_CountDown[0];
        m_CountDownImage.gameObject.SetActive(true);
        m_CountDownAnimator.SetTrigger("Count");
        
        yield return Timing.WaitForSeconds(1);
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_ReadyClip);
        m_Mulai.gameObject.SetActive(true);
        Game_Manager.m_Instance.f_RepositionPlayer();
        m_CountDownImage.gameObject.SetActive(false);
        
        yield return Timing.WaitForSeconds(1);

        f_ToogleJoystick(true);

        Game_Manager.m_Instance.m_GameState = e_GameState.IsPlaying;
        Game_Manager.m_Instance.f_SpawnFood();
        m_Mulai.gameObject.SetActive(false);
    }


    public void f_Reset() {
        for (int i = 0; i < m_JoinButtonObject.Count; i++) {
            if(m_JoinButtonObject[i].m_ToogleJoin) m_JoinButtonObject[i].f_MoveToInside();
        }
        //Timing.RunCoroutine(ie_CameraZoomOut());
    }
    public void f_ToogleJoystick(bool p_Toogle) {
        for (int i = 0; i < m_JoystickList.Count; i++) {
            if (Game_Manager.m_Instance.m_ListSnakes[i].m_PlayerState == e_PlayerState.Joined) {
                m_JoystickList[i].gameObject.SetActive(p_Toogle);
            }
        }
    }


}
