﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enumeration {
    public enum e_GameState
    {
        IsNotPlaying,
        IsPlaying
    }
    
    public enum e_PlayerState
    {
        NotJoined,
        Joined
    }
}