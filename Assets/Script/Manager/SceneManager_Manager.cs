﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MEC;
public class SceneManager_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static SceneManager_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_GameScene;
    public string m_MenuScene;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_Instance = this;
    }

    void Start() {
    }

    void Update() {

    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_LoadGame() {
        Timing.RunCoroutine(ie_ChangeScene(m_GameScene));
    }
    public void f_LoadMenu() {
        Timing.RunCoroutine(ie_ChangeScene(m_MenuScene));
    }
    public IEnumerator<float> ie_ChangeScene(string p_SceneName) {
        AsyncOperation t_AsyncOp = SceneManager.LoadSceneAsync(p_SceneName);

        while (!t_AsyncOp.isDone) {
            yield return Timing.WaitForOneFrame;
        }
    }
}
