﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AudioManager_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static AudioManager_Manager m_Instance;
    //===== STRUCT =====
    //===== PUBLIC =====
    public AudioSource m_OneTimeAudio;
    public AudioSource m_LoopAudio;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_Instance = this;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_PlayOneshot(AudioClip p_Clip) {
        m_OneTimeAudio.PlayOneShot(p_Clip);
        m_OneTimeAudio.PlayOneShot(p_Clip);

    }

    public void f_PlayOneshot(AudioClip p_Clip, float p_Volume) {
        m_OneTimeAudio.PlayOneShot(p_Clip, p_Volume);
        m_OneTimeAudio.PlayOneShot(p_Clip);

    }

    public void f_PlayLoop(AudioClip p_Clip) {
        m_LoopAudio.clip = p_Clip;
        m_LoopAudio.Play();
    }

    public void f_StopLoop() {
        m_LoopAudio.Stop();
    }
}
