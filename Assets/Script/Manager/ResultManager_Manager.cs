﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;

public class ResultManager_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ResultManager_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====
    [Header("Animation")]
    public GameObject m_Vignette;
    public GameObject m_Confenti1;
    public GameObject m_Confenti2;
    public GameObject m_ParticleConfenti;
    public GameObject m_WinnerText;
    public Transform m_TargetWinnerTxt;
    public GameObject m_Crown;
    public Transform m_TargetCrown;
    public GameObject m_Winner1Text;
    public GameObject m_Winner1Score;
    public GameObject m_Winner2Gameobject;
    public Transform m_TargetWinner2;
    public GameObject m_Winner3Gameobject;
    public Transform m_TargetWinner3;
    public GameObject m_Winner4Gameobject;
    public Transform m_TargetWinner4;
    public GameObject m_HomeButton;
    [Header("Score")]
    public List<TextMeshProUGUI> m_WinnerTitleText;
    public List<TextMeshProUGUI> m_WinnerScoreText;
    public List<string> m_WinnerTitle;
    public List<Color> m_TextColors;
    public List<Head_Movement> m_ListMIGA = new List<Head_Movement>(); // <==
    [Header("Audio")]
    public AudioClip m_ApplauseClip;
    public AudioClip m_PopperClip1;
    public AudioClip m_PopperClip2;
    public AudioClip m_WinnerClip;
    int m_Index;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
      //terak f_StartResultAppear();
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    
    public void f_StartResultAppear() { // <==
        f_AddScore();
        m_Index = 0;
        for (int i = 0; i < Game_Manager.m_Instance.m_ListSnakes.Count; i++) {
            if (Game_Manager.m_Instance.m_ListSnakes[i].m_PlayerState == e_PlayerState.Joined) {
                m_WinnerTitleText[m_Index].text = m_WinnerTitle[f_GetPlayerScore(m_Index)];
                m_WinnerTitleText[m_Index].color = m_TextColors[f_GetPlayerScore(m_Index)];
                m_WinnerScoreText[m_Index].text = "" + Game_Manager.m_Instance.m_ListSnakes[f_GetPlayerScore(m_Index)].f_GetScore();
                m_WinnerScoreText[m_Index].color = m_TextColors[f_GetPlayerScore(m_Index)];
                m_Index++;
            }
        }

        for (int j = m_Index; j < m_WinnerTitleText.Count; j++) {
            m_WinnerTitleText[j].gameObject.SetActive(false);
            m_WinnerScoreText[j].gameObject.SetActive(false);
        }

        Timing.RunCoroutine(ie_VigneteAppear());
    }

    public void f_AddScore() { // <==
        Head_Movement t_Score;

        for (int i = 0; i < Game_Manager.m_Instance.m_ListSnakes.Count; i++) {
            m_ListMIGA.Add(Game_Manager.m_Instance.m_ListSnakes[i]);
        }

        for (int i = 0; i < m_ListMIGA.Count; i++) {
            for (int j = i; j < m_ListMIGA.Count; j++) {
                if (m_ListMIGA[i].f_GetScore() < m_ListMIGA[j].f_GetScore()) {
                    t_Score = m_ListMIGA[i];
                    m_ListMIGA[i] = m_ListMIGA[j];
                    m_ListMIGA[j] = t_Score;
                }
            }
        }
    }

    public int f_GetPlayerScore(int p_Ranking) { // <==
        for (int i = 0; i < Game_Manager.m_Instance.m_ListSnakes.Count; i++) {
            if(Game_Manager.m_Instance.m_ListSnakes[i] == m_ListMIGA[p_Ranking]) {
                return i;
            }
        }

        return -1;
    }

    IEnumerator<float> ie_VigneteAppear() {
        yield return Timing.WaitForSeconds(1);
        m_Vignette.gameObject.SetActive(true);
        iTween.ScaleTo(m_Vignette, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 1f, "easetype", iTween.EaseType.linear, "oncomplete", "f_ConfentiAppear", "oncompletetarget", gameObject));
    }

    public void f_ConfentiAppear() {
        Timing.RunCoroutine(ie_ConfentiShow());
    }

    IEnumerator<float> ie_ConfentiShow() {
        m_ParticleConfenti.gameObject.SetActive(true);

        yield return Timing.WaitForSeconds(1f);

        m_Confenti1.gameObject.SetActive(true);
        m_Confenti2.gameObject.SetActive(true);

        AudioManager_Manager.m_Instance.f_PlayOneshot(m_PopperClip1);
        yield return Timing.WaitForSeconds(.1f);
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_PopperClip2);

        yield return Timing.WaitForSeconds(.5f);

        iTween.MoveTo(m_Crown, iTween.Hash("position", m_TargetCrown.position, "time", 1f, "easetype", iTween.EaseType.easeInOutBounce, "oncomplete", "f_WinnerTextAppear", "oncompletetarget", gameObject));
    }

    public void f_WinnerTextAppear() {
        iTween.MoveTo(m_WinnerText, iTween.Hash("position", m_TargetWinnerTxt.position, "time", 1f, "easetype", iTween.EaseType.easeInOutBounce, "oncomplete", "f_Winner1TextAppear", "oncompletetarget", gameObject));
    }

    public void f_Winner1TextAppear() {
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_WinnerClip);
        AudioManager_Manager.m_Instance.f_PlayOneshot(m_ApplauseClip);
        m_Winner1Text.gameObject.SetActive(true);
        iTween.ScaleTo(m_Winner1Text, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 1f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "f_Winner1ScoreAppear", "oncompletetarget", gameObject));
    }

    public void f_Winner1ScoreAppear() {
        m_Winner1Score.gameObject.SetActive(true);
        iTween.ScaleTo(m_Winner1Score, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 1f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "f_Winner2TextAppear", "oncompletetarget", gameObject));
    }

    public void f_Winner2TextAppear() {
        iTween.MoveTo(m_Winner2Gameobject, iTween.Hash("position", m_TargetWinner2.position, "time", 1f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "f_Winner3TextAppear", "oncompletetarget", gameObject));
    }
    public void f_Winner3TextAppear() {
        iTween.MoveTo(m_Winner3Gameobject, iTween.Hash("position", m_TargetWinner3.position, "time", 1f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "f_Winner4TextAppear", "oncompletetarget", gameObject));
    }
    public void f_Winner4TextAppear() {
        iTween.MoveTo(m_Winner4Gameobject, iTween.Hash("position", m_TargetWinner4.position, "time", 1f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "f_HomeButtonAppear", "oncompletetarget", gameObject));
    }
    public void f_HomeButtonAppear() {
        m_HomeButton.gameObject.SetActive(true);
       //TransitionCover_Gameobject.m_Instance.m_OnCloseDone.AddListener(SceneManager_Manager.m_Instance.f_Load);
    }
}
